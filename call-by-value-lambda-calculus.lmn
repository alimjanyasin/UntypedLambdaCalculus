/*
Implementing Call-by-value using Token-Passing technique.

see paper "Call-by-Name and Call-by-Value as Token-Passing Interaction Nets"

*/




/*

Hypergraph representation of lambda-terms

Alimjan Yasin

2015-12
*/

iden@@ N=iden :- new(X,1) | N=lam(X,X).
no0@@ N=n(0) :-  new(X,1),new(F,1) | N=lam(F, lam(X, X)).
no1@@ N=n(1) :- new(F,1),new(X,1) | N=lam(F,lam(X,app(F,X))).
no2@@ N=n(2) :- new(F,1),new(Z,1) | N=lam(F,lam(Z,app(F,app(F,Z)))).  
no3@@ N=n(3) :- new(F,1),new(Z,1) | N=lam(F,lam(Z,app(F,app(F,app(F,Z))))).	
no4@@ N=n(4) :- new(F,1),new(Z,1) | N=lam(F,lam(Z,app(F,app(F,app(F,app(F,Z)))))).	
no5@@ N=n(5) :- new(F,1),new(Z,1) | N=lam(F,lam(Z,app(F,app(F,app(F,app(F,app(F,Z))))))).	
no10@@ N=n(10) :- new(F,1),new(Z,1) | N=lam(F,lam(Z,app(F,app(F,app(F,app(F,app(F,app(F,app(F,app(F,app(F,app(F,Z)))))))))))).	


% put eval as the top constructor to run examples

/*results of all the inputs are carefully checked, all correct.
Call-by-value reference: http://zaa.ch/lambdacalc/
*/


%init:-r=eval(app(n(2),n(2))). 
%init:-r=eval(app(app(n(2),n(2)),n(2))). 
%init:-r=eval(app(n(3),n(3))). 
%init:-r=eval(app(app(n(3),n(2)),n(2))).  
%init:-r=eval(app(app(n(2),n(2)),n(3))).  
%init:-r=eval(app(n(4),n(4))). 


%init:-r=eval(app(app(app(n(2),n(2)),iden),iden)). 
%init:-r=eval(app(app(app(app(n(2),n(2)),n(2)),iden),iden)).
%init:-r=eval(app(app(app(n(3),n(3)),iden),iden)). 
%init:-r=eval(app(app(app(app(n(3),n(2)),n(2)),iden),iden)). 
%init:-r=eval(app(app(app(app(n(2),n(2)),n(3)),iden),iden)). 
%init:-r=eval(app(app(app(n(4),n(4)),iden),iden)). 



/********** implementations */

init.

/* call-by-value*/

R=eval(lam(A,B)) :- R=value(lam(A,B)).
R=eval(app(A,B)) :- R=app(eval(A),B).
R=app(value(A),B):-R=app(A,eval(B)).
beta@@ R=app(lam(X,A),value(B)) :- R=eval(subs(A,X,B)).   

/*substitution */

sub_var1@@ subs(X,X,N,R)          :- hlink(X)|R=N. 
sub_var2@@ subs(X,Y,N,R)          :- hlink(X),X\=Y, hlground(N,1)|R=X.
sub_abs @@ subs(lam(X,M),Y,N,R)   :- lam(X,subs(M,Y,N),R).
sub_app @@ subs(app(M1,M2),X,N,R) :- hlink(X),hlground(N,1)| 
			                   app(subs(M1,X,N),subs(M2,X,N),R). 

